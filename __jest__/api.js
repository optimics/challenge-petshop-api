import getPort from 'get-port'

import { configure } from 'djorm/config.js'
import { copyFile } from 'fs/promises'
import { dirname, resolve } from 'path'
import { file } from 'tmp-promise'
import { fileURLToPath } from 'url'
import { getModel } from 'djorm/models/index.js'
import { networkInterfaces } from 'os'
import { start } from '../server'

const fetch = jest.requireActual('node-fetch')
const DIR_NAME = dirname(fileURLToPath(import.meta.url))

export const setupDb = ({ dbSource, config = {} }) => {
  beforeEach(async () => {
    config.dbFile = await file()
    await copyFile(dbSource, config.dbFile.path)
  })

  afterEach(async () => {
    await config.dbFile.cleanup()
  })

  return config
}

const loadFixture = async fixturePath => {
  const records = require(fixturePath)
  for (const record of records) {
    await getModel(record.model).create(record.props)
  }
}

const loadFixtures = async (...fixturePaths) => {
  for (const fixturePath of fixturePaths) {
    await loadFixture(fixturePath)
  }
}

const prepareFixtures =
  (...fixturePaths) =>
  async () =>
    await loadFixtures(...fixturePaths)

const mapNetConfig = netConfig =>
  netConfig.family === 'IPv6' ? `[${netConfig.address}]` : netConfig.address

export const setupApi = ({ dbSource, config = {} }) => {
  setupDb({ config, dbSource })

  beforeEach(async () => {
    config.port = await getPort()
    configure({
      apiPort: config.port,
      logger: {
        level: 'warn',
      },
      databases: {
        default: {
          driver: 'djorm-db-sqlite',
          path: config.dbFile.path,
        },
      },
    })
    const addrs = Object.values(networkInterfaces()).flat().map(mapNetConfig)
    config.app = await start()
    config.app.hostOrigin = `http://${addrs[0]}:${config.port}`
  })

  afterEach(async () => {
    await config.app.close()
  })

  config.fetch = async (path, options = {}) =>
    await fetch(`${config.app.hostOrigin}${path}`, {
      ...options,
      headers: {
        'Content-Type': 'application/json',
        ...options.headers,
      },
    })
  return config
}

export const setupCommonApi = (...fixturePaths) => {
  const config = {}

  setupApi({
    config,
    dbSource: resolve(DIR_NAME, '..', 'db', 'empty.sqlite'),
  })

  beforeEach(prepareFixtures(...fixturePaths))

  afterEach(() => {
    jest.clearAllMocks()
    jest.resetAllMocks()
  })

  return config
}
