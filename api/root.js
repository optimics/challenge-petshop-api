import { readFile } from 'fs/promises'

const readJsonFile = async path =>
  JSON.parse(await readFile(new URL(path, import.meta.url)))

export const root = fastify => {
  fastify.get('/', async () => {
    const npm = await readJsonFile('../package.json')
    return {
      name: npm.name,
    }
  })
  return Promise.resolve()
}
