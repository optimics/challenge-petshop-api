module.exports = {
  env: {
    node: true,
    es6: true,
  },
  settings: {
    react: {
      version: '16.3',
    },
  },
  extends: ['@optimics/eslint-config'],
  overrides: [
    {
      files: ['**/__jest__/*js', '**/__tests__/*js'],
      env: {
        jest: true,
      },
    },
  ],
}
