import { advanceTo } from 'jest-date-mock'
import { init } from 'djorm/config.js'
import { Person, Pet, Reservation } from '../../db/models.js'
import { setupCommonApi } from '../../__jest__/api.js'

describe('reservations api', () => {
  const config = setupCommonApi()

  beforeEach(() => {
    advanceTo(new Date(2022, 2, 14, 0, 0, 0))
  })

  it('returns empty list of reservations', async () => {
    const res = await config.fetch('/reservations')
    expect(await res.json()).toEqual({ _data: [] })
  })

  describe('with existing reservations', () => {
    beforeEach(async () => {
      await init()
      const pet = await Pet.create({
        animal: 'dog',
        name: 'Jack',
      })
      await Pet.create({
        animal: 'dog',
        name: 'Jerry',
      })
      const person = await Person.create({
        name: 'Daniel',
      })
      await Reservation.create({
        ownerId: person.id,
        petId: pet.id,
        since: '2022-03-14T15:00:00Z',
        until: '2022-03-14T16:00:00Z',
      })
      await Reservation.create({
        ownerId: person.id,
        petId: pet.id,
        since: '2022-03-15T15:00:00Z',
        until: '2022-03-15T16:00:00Z',
      })
    })

    it('listing returns a set of existing reservations', async () => {
      const res = await config.fetch('/reservations')
      expect(await res.json()).toEqual({
        _data: [
          expect.objectContaining({
            ownerId: 1,
            petId: 1,
            status: 1,
            since: '2022-03-14T15:00:00.000Z',
            until: '2022-03-14T16:00:00.000Z',
          }),
          expect.objectContaining({
            ownerId: 1,
            petId: 1,
            status: 1,
            since: '2022-03-15T15:00:00.000Z',
            until: '2022-03-15T16:00:00.000Z',
          }),
        ],
      })
    })

    describe('given pet reservation start clearly overlaps with another', () => {
      let res
      beforeEach(async () => {
        res = await config.fetch('/reservations', {
          method: 'POST',
          body: JSON.stringify({
            ownerId: 1,
            petId: 1,
            since: '2022-03-14T15:30:00.000Z',
            until: '2022-03-14T16:30:00.000Z',
          }),
        })
      })

      it('creating returns overlaps message', async () => {
        expect(await res.json()).toMatchObject({
          fields: [
            {
              code: 'reservation-overlaps',
              field: 'since',
              value: '2022-03-14T15:30:00.000Z',
              message: 'Field "since" overlaps with another reservation',
            },
          ],
        })
      })

      it('creating returns HTTP 400', () => {
        expect(res.status).toBe(400)
      })
    })

    describe('given pet reservation end clearly overlaps with another', () => {
      let res
      beforeEach(async () => {
        res = await config.fetch('/reservations', {
          method: 'POST',
          body: JSON.stringify({
            ownerId: 1,
            petId: 1,
            since: '2022-03-14T14:30:00.000Z',
            until: '2022-03-14T15:30:00.000Z',
          }),
        })
      })

      it('creating returns overlaps message', async () => {
        expect(await res.json()).toMatchObject({
          fields: [
            {
              code: 'reservation-overlaps',
              field: 'until',
              value: '2022-03-14T15:30:00.000Z',
              message: 'Field "until" overlaps with another reservation',
            },
          ],
        })
      })

      it('creating returns HTTP 400', () => {
        expect(res.status).toBe(400)
      })
    })

    describe('given pet reservation ends on the same date as another starts', () => {
      let res
      beforeEach(async () => {
        res = await config.fetch('/reservations', {
          method: 'POST',
          body: JSON.stringify({
            ownerId: 1,
            petId: 1,
            since: '2022-03-14T14:00:00.000Z',
            until: '2022-03-14T15:00:00.000Z',
          }),
        })
      })

      it('creating returns the new reservation', async () => {
        expect(await res.json()).toMatchObject({
          ownerId: 1,
          petId: 1,
          since: '2022-03-14T14:00:00.000Z',
          until: '2022-03-14T15:00:00.000Z',
        })
      })

      it('creating returns HTTP 200', () => {
        expect(res.status).toBe(201)
      })
    })

    describe('given pet reservation starts on the same date as another ends', () => {
      let res
      beforeEach(async () => {
        res = await config.fetch('/reservations', {
          method: 'POST',
          body: JSON.stringify({
            ownerId: 1,
            petId: 1,
            since: '2022-03-14T16:00:00.000Z',
            until: '2022-03-14T17:00:00.000Z',
          }),
        })
      })

      it('creating returns overlaps message', async () => {
        expect(await res.json()).toMatchObject({
          ownerId: 1,
          petId: 1,
          since: '2022-03-14T16:00:00.000Z',
          until: '2022-03-14T17:00:00.000Z',
        })
      })

      it('creating returns HTTP 200', () => {
        expect(res.status).toBe(201)
      })
    })

    describe('given pet reservation end overlaps with another but to a different pet', () => {
      let res
      beforeEach(async () => {
        res = await config.fetch('/reservations', {
          method: 'POST',
          body: JSON.stringify({
            ownerId: 1,
            petId: 2,
            since: '2022-03-14T14:30:00.000Z',
            until: '2022-03-14T15:30:00.000Z',
          }),
        })
      })

      it('creating returns the new reservation', async () => {
        expect(await res.json()).toMatchObject({
          ownerId: 1,
          petId: 2,
          since: '2022-03-14T14:30:00.000Z',
          until: '2022-03-14T15:30:00.000Z',
        })
      })

      it('creating returns HTTP 201', () => {
        expect(res.status).toBe(201)
      })
    })
  })
})
